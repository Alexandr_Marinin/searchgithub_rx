//
//  SearchGitHubRepoMiddleware.swift
//  SearchGitHubTest
//
//  Created by 18188930 on 24.03.2020.
//  Copyright © 2020 18188930. All rights reserved.
//

import Foundation
import ReSwift
import RxSwift
import Alamofire

func searchGitHub(searchText: String) -> Observable<[Item]> {
    return Observable<[Item]>.create() {
        observer in
        let req1 = "https://api.github.com/search/repositories?q=" + (searchText)
        
        //сделать что-бы здесь 2 запроса формировались через rxalmofire
        
        Alamofire.request(req1).responseJSON()
            { response in
                switch response.result {
                case .success :
                    do {
                        let data =  try JSONDecoder().decode( GithubSearchRequest.self, from: response.data!)
                        if let items = data.items {
                            observer.onNext(items)
                        } else {
//                            print("no req items")
//                            observer.onError(SearchGitHub.error)
                            observer.onNext([])
                        }
                        
                    } catch  {
                        print("Error validate Json")
                        break
                    }
                case .failure :
                    print("Failure response")
                    break
                }
        }
        
        //тут объдинялись например через zip
        
        //тут же на них подписывался, и возвращать правильный observer.next
        
        return Disposables.create()
    }
}

let searchGitHubRepoMiddleware : Middleware<AppState> = { dispatch, getState in
    return { next in
        return { action in
            // perform middleware logic
            switch action {
            case let value as SearchReq:
                searchGitHub(searchText: value.searchText).asObservable().subscribe(onNext:
                    {
                        next(SearchGitHub.sucess($0))
                })
            default:
                break
            }
            
            // call next middleware
            return next(action)
        }
    }
}

//MARK: decode struct

struct GithubSearchRequest : Codable {
    
    var incomplete_results : Bool?
    var items : [Item]?
    var total_count : Int?
}
