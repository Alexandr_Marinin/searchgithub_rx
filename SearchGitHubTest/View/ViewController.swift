//
//  ViewController.swift
//  SearchGitHubTest
//
//  Created by 18188930 on 16.03.2020.
//  Copyright © 2020 18188930. All rights reserved.
//

import UIKit
import ReSwift
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let textFieldTableView = Variable<[Item]?>([])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
        let disposeBag = DisposeBag()
        
        //связываем окно поиска (наблюдаемое), с переменной (наблюдателем)
        searchBar.rx.text.orEmpty
            .throttle(.seconds(5), scheduler: MainScheduler.instance).distinctUntilChanged()
            .subscribe(onNext: {
                store.dispatch(SearchReq(searchText: $0))
            })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subcription in
            subcription.select { state in return state.searchTableViewState }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
}

extension ViewController : StoreSubscriber {
    typealias StoreSubscriberStateType = SearchTableViewState
    
        func newState(state: SearchTableViewState) {
            self.textFieldTableView.value = state.tableView
            self.tableView.reloadData()
        }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textFieldTableView.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell")
        
        cell?.textLabel?.text = textFieldTableView.value?[indexPath.row].name ?? ""
        
        return cell ?? UITableViewCell()
    }
}
