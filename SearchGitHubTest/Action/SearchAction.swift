//
//  SearchAction.swift
//  SearchGitHubTest
//
//  Created by 18188930 on 24.03.2020.
//  Copyright © 2020 18188930. All rights reserved.
//

import Foundation
import ReSwift

enum SearchGitHub : Action {
    case sucess ([Item]?)
    case loading
    case error
}

struct SearchReq : Action {
    let searchText : String
}
