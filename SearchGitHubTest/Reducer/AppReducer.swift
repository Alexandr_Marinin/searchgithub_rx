//
//  AppReducer.swift
//  SearchGitHubTest
//
//  Created by 18188930 on 24.03.2020.
//  Copyright © 2020 18188930. All rights reserved.
//

import Foundation
import ReSwift

func reducerSearchTableViewState(action: Action, state: SearchTableViewState?) -> SearchTableViewState {
    var state = state ?? SearchTableViewState()
    
    switch action {
    case let sender as SearchGitHub:
        switch sender {
        case .sucess(let item):
            state.tableView = item
        case .loading:
            print("loading")
        case .error:
            state.tableView = nil
        }
    default:
        break
    }
    
    return state
}

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(searchTableViewState: reducerSearchTableViewState(action: action, state: state?.searchTableViewState))
}
