//
//  AppState.swift
//  SearchGitHubTest
//
//  Created by 18188930 on 24.03.2020.
//  Copyright © 2020 18188930. All rights reserved.
//

import Foundation
import ReSwift
import RxSwift

struct AppState : StateType {
//    let searchTableViewState : [Item]
    var searchTableViewState : SearchTableViewState!
}

struct SearchTableViewState : StateType {
    var tableView : [Item]?
}

struct Item : Codable {
    
    //    var owner : Owner?
    var name : String?
    var fullName : String?
    var language : String?
    var url : String?
}

//struct Owner : Codable {
//
//    var login : String?
//}
