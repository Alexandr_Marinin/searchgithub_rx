//
//  AppStory.swift
//  SearchGitHubTest
//
//  Created by 18188930 on 24.03.2020.
//  Copyright © 2020 18188930. All rights reserved.
//

import Foundation
import ReSwift
import RxSwift

//let store = Store(reducer: appReducer, state: AppState(searchTableViewState: nil))
let store = Store(reducer: appReducer, state: AppState(searchTableViewState: nil), middleware: [searchGitHubRepoMiddleware])
